/*
 * EE 422C - Spring 2015
 * Assignment 5
 * Robbie Zuazua, Mehtaab Brar, James Yu
 */

package assignment5;

public class NoSuchLadderException extends Exception
{
    private static final long serialVersionUID = 1L;

    public NoSuchLadderException(String message)
    {
        super(message);
    }

    public NoSuchLadderException(String message, Throwable throwable)
    {
        super(message, throwable);
    }
}
