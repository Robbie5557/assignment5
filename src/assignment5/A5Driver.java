/*
 * EE 422C - Spring 2015
 * Assignment 5
 * Robbie Zuazua, Mehtaab Brar, James Yu
 */

package assignment5;

import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Scanner;

/**
 * Driver class, project starts here
 * @param : input text file
 * */
public class A5Driver
{
	// used to convert nanoseconds to seconds
	private static final double NANOS_PER_SEC = 1000000000.0; 
	static Assignment5Interface wordLadderSolver;
	static String inputFilename; 
	static HashSet<String> dictionary;
	
    public static void main(String[] args)
    {
        // this string specifies the name of the input file with word pairs
    	// this file should be in the working directory of the program, else the program will exit
    	inputFilename = "assn5data.txt";
    	
    	// Create a word ladder solver object, which will be used for all the pairs in the input file
        wordLadderSolver = new WordLadderSolver();
        
        // copy the wordLadderSolver class member to a local variable for ease of use
        dictionary = new HashSet<String>();
        dictionary = wordLadderSolver.getDictionary();
    	  	
        // create stop-watch and variables to track execution time
        StopWatch s = new StopWatch();
        long mytime;        
        s.start();
        
    	// Read input file. need to pass your wordLadderSolver object
    	processLinesInFile (inputFilename);
    	
    	// report stop-watch metrics
    	s.stop();
    	mytime = s.getElapsedTime();
    	System.out.println("Time elapsed is: " + mytime/NANOS_PER_SEC + " seconds");	
    }
	
    /**
     * Opens the file specified in String filename, reads each line in it
     * will create a wordLadder if one exists for every pair of legal words in the file given
     * @param filename, wordLadderSolver - the name of the file that needs to be read, the object which is gonna create those ladders
     * @throws NoSuchWordException if a word in the input file does not exist in the dictionary
     */
    public static void processLinesInFile (String filename)
	{ 
    	try 
		{
    		// read the input file from working directory
			Scanner inFile = new Scanner(new File(filename));
			
			// while there is still data to be read 
			while (inFile.hasNextLine())
			{
				try 
				{
					String word1 = inFile.next();
					String word2 = inFile.next();
					
					//check if both the words are in the dictionary, if not throw exception
					if(!(dictionary.contains(word1) && dictionary.contains(word2))) 
					{
						throw new NoSuchWordException("At least one of the words " + word1 + " and " + word2 + " are not legitmate 5 letter words from the dictionary"); 
					}
					try 
					{
						// this method will create the word ladder and put it in result
						List<String> result = wordLadderSolver.computeLadder(word1, word2);
						
						//optional method to check if our word Ladder Solver is correct
						boolean correct = wordLadderSolver.validateResult(word1, word2, result);
						//if(!correct) System.out.println("The ladder between " + word1 + " and " + word2 + " is not valid");
						
						// if there are words in the ladder, they will be printed
						// if there are no words in the ladder, the NoSuchLadderException will be thrown and caught
						for(String f: result) System.out.println(f);
						
						// clear the variables
						wordLadderSolver.clear();
					}
					
					// handles the case in which there is no valid ladder between the two words
					catch (NoSuchLadderException e) 
					{
						System.out.println(e.getMessage());
					}
				}
				
				// if the words are not legitimate 5 letter words in the dictionary
				catch(NoSuchWordException e)
				{
					System.out.println(e.getMessage());
				}

				// handle empty lines, or extra words at end of current line
				if(inFile.hasNextLine()) 
				{
					inFile.nextLine();
				}
				
				//  delimiter between ladders
				System.out.println("**********");
				
			}
			inFile.close();
		} 
		catch (IOException e) 
		{
			System.err.println ("Error: IO exception. Exiting...");
			e.printStackTrace();
			System.exit(-1);
		}
	}	
    		
    	
}
