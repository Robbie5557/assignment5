/*
 * EE 422C - Spring 2015
 * Assignment 5
 * Robbie Zuazua, Mehtaab Brar, James Yu
 */

package assignment5;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;

public class WordLadderSolver implements Assignment5Interface
{
	/* list of words in ladder between two given words */
	private List<String> SolutionList;
	
	/* the words in the provided dictionary file */
	private static HashSet<String> dictionary;
	
	/* contains the same words as dictionary, but easier to loop through */
	private List<String> dictionary_list;	
	
	/* words that have already been visited as candidates in the search for a ladder 
	 * prevents endless recursion caused by re-visiting words */
	private HashSet<String> seenWords;
	
	/* name of the dictionary data file.
	 * make sure the file is in the working directory of the program 
	 */
	private String dictionaryFilename = "dictionary.dat";
	
    /**
     * Constructor for WordLadderSolver
     * Instantiates class members, and populates dictionary
     */
	public WordLadderSolver()
	{
		SolutionList = new ArrayList<String>();
		dictionary = new HashSet<String>();
		processDictionary (dictionaryFilename);
		dictionary_list = new ArrayList<String>(dictionary);
		seenWords = new HashSet<String>();
	}
	
	/**
	 * clear the lists that are used to track observed words and ladder words
	 * these lists are cleared and re-populated on each iteration of computeLadder
	 */
	public void clear()
	{
		SolutionList.clear();
		seenWords.clear();
	}
	
    // do not change signature of the method implemented from the interface
	/**
	 * Computes the word ladder from startWord to endWord.
	 * @param startWord The starting word in the word ladder.
     * @param endWord The ending word in the word ladder.
     * @return A list of strings that represents the word ladder. The 0th index should contain
     * the startWord and the last position should contain endWord. All intermediate words should
     * be different by exactly one letter.
     * @throws NoSuchLadderException is thrown if no word ladder can be generated from startWord and endWord.
	 * 
	 * */
    @Override
    public List<String> computeLadder(String startWord, String endWord) throws NoSuchLadderException 
    {
        List<String> ladder = makeLadder(startWord, endWord, -1);
    	if(ladder.isEmpty()) throw new NoSuchLadderException("No valid word ladder could be found between " + startWord + " and " + endWord);
    	return ladder;
    }

    /**
     * optional method to make sure your ladder is correct
     * @param startWord The starting word in the word ladder.
     * @param endWord The ending word in the word ladder.
     * @param A list of strings that represents the word ladder. The 0th index should contain
     * the startWord and the last position should contain endWord. All intermediate words should
     * be different by exactly one letter.
	 * 
     * */
    @Override
    public boolean validateResult(String startWord, String endWord, List<String> wordLadder) 
    {
        if(!(wordLadder.get(0).equals(startWord) && wordLadder.get(wordLadder.size()-1).equals(endWord)))
        	return false;
        for(int i = 0; i < wordLadder.size() - 1; i++)
        {
        	String word1 = wordLadder.get(i);
        	String word2 = wordLadder.get(i + 1);
        	// possibly redundant check for words in dictionary, but contains() is O(1) for HashSet so whatever
        	if(!(dictionary.contains(word1) && dictionary.contains(word2)))
    			return false;
        	// check that difference between words is exactly one letter
        	if(compareWords(word1, word2, -1) == -1)
        		return false;
        	
        }
    	return true;
    }

    /**
     * recursive method that creates the word ladder 
     * @param startWord The starting word in the word ladder.
     * @param endWord The ending word in the word ladder.
     * @param int - the last position that was changed 
     * @return A list of strings that represents the word ladder. The 0th index should contain
     * the startWord and the last position should contain endWord. All intermediate words should
     * be different by exactly one letter.
	 * 
     * */   
    public List<String> makeLadder(String startWord, String endWord, int pos)
    {
    	// base case
    	if(compareWords(startWord, endWord, pos) != -1) 
    	{
    		SolutionList.add(startWord);
    		SolutionList.add(endWord);
    		return SolutionList; 
    	}
    	// candidates are all the words that are one position away from the start word
    	SortedSet<String> candidates = new TreeSet<String>();
    	SolutionList.add(startWord);
    	
    	// find all the viable candidate words in the dictionary
    	// i.e. words that have not been traversed before, and are one position away from he the start word
    	// also add the viable candidates to a seenWords list, so that endless recursion is avoided
    	for(int i = 0; i < dictionary.size(); i++)
    	{
    		String dicWord = dictionary_list.get(i);
    		int x = compareWords(startWord, dicWord, pos);
    		if(x != -1)
    		{
    			// check that word is not in solution list and candidates list
    			if(!(SolutionList.contains(dicWord) || seenWords.contains(dicWord)))
    			{
    				int diff = diffWords(dicWord, endWord);
    				candidates.add(diff + dicWord);
    				seenWords.add(dicWord);
    			}
    		}
    	}
    	
    	// check all the candidates to see if any one of them leads to an end word
    	for(String candidate : candidates){
    		
    		if (candidate == null)
    		{
    			return SolutionList;
    		}
    		
    		// strip prepended number
    		candidate = candidate.replaceAll("[0-9]","");
    		
    		// get position last changed between current candidate and last candidate (startWord)
    		int x = compareWords(startWord, candidate, pos);
    		SolutionList = makeLadder(candidate, endWord,x);
    		
    		// done, solution list is the appropiate wordLadder
    		if (SolutionList.contains(endWord))
    		{
    			return SolutionList;
    		}
    	}
    	// remove the added words from solution list if no solution was found
    	if (!SolutionList.contains(endWord))
		{
			SolutionList.remove(startWord);
		}
		return SolutionList;
    }
    /**
     * Accessor for the dictionary class member 
     * @return HashSet containing the parsed dictionary
     * */
    public HashSet<String> getDictionary()
    {
    	return dictionary;
    }
    
    /**
     * Parses dictionary data file and populates the class member, dictionary, with its contents
     * @param filename the name of the dictionary data file
     */
    
    public static void processDictionary (String filename) 
	{ 
		try 
		{
			FileReader freader = new FileReader(filename);
			BufferedReader reader = new BufferedReader(freader);
			
			for (String s = reader.readLine(); s != null; s = reader.readLine()) 
			{
				// make sure word does not start with * and the word is at least 5 letters long
				if ((!s.startsWith("*"))&& s.length() >= 5)
				{
					dictionary.add(s.substring(0,5));
				}
			}
			reader.close();
		} 
		catch (FileNotFoundException e) 
		{
			System.err.println ("Error: File not found. Exiting...");
			e.printStackTrace();
			System.exit(-1);
		} 
        catch (IOException e) 
		{
			System.err.println ("Error: IO exception. Exiting...");
			e.printStackTrace();
			System.exit(-1);
		}
    }
    
    /**
     * returns either the position (if the difference between two words is 1)
     * or -1 if the words have more than one difference
     * @param startWord The starting word in the word ladder.
     * @param endWord The ending word in the word ladder.
     * @param int - the last position that was changed 
     * @return integer position or -1.
     * 
     */
    public int compareWords(String word1, String word2, int pos)
    {
    	int diff = 0;
    	int temp = -1;
    	for(int i = 0; i < word1.length(); i++)
    	{
    		if(word1.charAt(i) != word2.charAt(i)) 
    		{
    			diff++;
    			temp = i;
    		}
    	}
    	if(diff == 1 && temp != pos) return temp;
    	else return -1;
    }
    /**
     * returns the number of differences between the word and the end word. 
     * We will then prepend this number to the word and add this into a sorted tree set 
     * this will help make our algorithm faster since the words that have a greater chance
     * of reaching the end goal are searched first
     * @param startWord The starting word in the word ladder.
     * @param endWord The ending word in the word ladder.
     * @param int - the last position that was changed 
     * @return integer number of differences
     * 
     */
    public int diffWords(String word1, String target)
    {
    	int diff = 0;
    	for(int i = 0; i < word1.length(); i++)
    	{
    		if(word1.charAt(i) != target.charAt(i)) diff++;
    	}
    	return diff;
    }
}
