/*
 * EE 422C - Spring 2015
 * Assignment 5
 * Robbie Zuazua, Mehtaab Brar, James Yu
 */

package assignment5;
/**
 * Exception is thrown when the input words is not a legitimate 5 letter word in dictionary 
 * */
public class NoSuchWordException extends Exception
{
	private static final long serialVersionUID = 1L;
	
	public NoSuchWordException(String message)
    {
        super(message);
    }

    public NoSuchWordException(String message, Throwable throwable)
    {
        super(message, throwable);
    }
	
}
